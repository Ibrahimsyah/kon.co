<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Kon.co</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('css/login_register.css') ?>">
	<link href="https://fonts.googleapis.com/css2?family=Baloo+Bhai+2:wght@700&display=swap" rel="stylesheet">
</head>

<body>
	<div class="header">
		<div class="container">
			<div class="row">
				<div class="col col-isi col-logo">
					Kon.Co
				</div>
				<div class="col col-isi col-form">
					<table class="table-form">
						<tr>
							<?php
							if (isset($loginError)) {
								echo "<td>
								<span style='color: #f03434; font-weight:bold; font-size:1em'>$loginError</span>
								</td>";
							}?>
							<form action="<?=base_url('/login')?>" method="POST">
								<td>
									<input required type="text" name="userLogin" id="userLogin" class="form-login" placeholder="Username / Email">
								</td>
								<td>
									<input required type="password" name="passwordLogin" id="passwordLogin" class="form-login" placeholder="Password">
								</td>
								<td>
									<button class="btn-login form-login" type="submit">Login</button>
								</td>
							</form>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="body">
		<div class="container">
			<div class="row">
				<div class="col">
					<img class="img-blob" src="<?php echo base_url('assets/blob_login.svg') ?>" alt="blob">
					<img class="img-icon" src="<?php echo base_url('assets/social_friend.svg') ?>" alt="icon">
					<p class="motto">Temukan teman dengan nyaman!</p>
				</div>
				<div class="col">
					<table class="table-register">
						<form action="<?=base_url('/register')?>" method="post">
							<tr>
								<td>
									<h1>Register</h1>
									<?php
										if (isset($registerError)) {
											echo "<div class='error'>Username telah digunakan, coba lagi</div>";
										}
									?>
								</td>
							</tr>
							<tr>
								<td><input required type="text" name="namaLengkap" id="namaLengap" class="form-register input-register" placeholder="Nama Lengkap"></td>
							</tr>
							<tr>
								<td><input required type="text" name="usernameRegister" id="usernameRegister" class="form-register input-register" placeholder="Username"></td>
							</tr>
							<tr>
								<td><input required type="email" name="email" id="email" class="form-register input-register" placeholder="Email"></td>
							</tr>
							<tr>
								<td><input required type="password" name="passwordRegister" id="passwordRegister" class="form-register input-register" placeholder="Password"></td>
							</tr>
							<tr>
								<td>
									<br>
									<h3>Tanggal Lahir:</h3>
								</td>
							</tr>
							<tr>
								<td><select name="tanggal" id="tanggal" class="form-register select-register">
										<?php
for ($i = 1; $i <= 31; $i++) {
    echo "<option value='$i'>$i</option>";
}
?>
									</select>
									<select name="bulan" id="bulan" class="form-register select-register">
										<?php
for ($i = 1; $i <= 12; $i++) {
    echo "<option value='$i'>$i</option>";
}
?>
									</select>
									<select name="tahun" id="tahun" class="form-register select-register">
										<?php
$currentYear = date("Y");
for ($i = $currentYear; $i >= $currentYear - 30; $i--) {
    echo "<option value='$i'>$i</option>";
}
?>
									</select></td>
							</tr>
							<tr>
								<td>
									<br>
									<h3>Jenis Kelamin:</h3>
								</td>
							</tr>
							<tr>
								<td><input type="radio" name="jk" id="lakiLaki" class="radio-kelamin" value="L">Laki-laki
									<input type="radio" name="jk" id="perempuan" class="radio-kelamin" value="P">Perempuan</td>
							</tr>
							<tr>
								<td><br><button type="submit" class="form-register btn-register">Daftar</button></td>
							</tr>
						</form>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>

</html>
