<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Kon.co</title>
    <link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" href="<?php echo base_url('css/akun.css') ?>" />
</head>

<body>
    <div class="row" style="margin: 0; height: 100%;">
        <div class="col-3">
            <div id="sidebarLeft" style="display: flex; flex-direction:column; justify-content: space-between;">
                <a href="<?= base_url('/') ?>">
                    <h3 class="title">Kon.co</h3>
                </a>
                <button class="btn-logout form-logout" type="submit" id='logout'>Logout</button>
            </div>
        </div>
        <div class="col-6" style="padding:0">
            <img src="<?php echo base_url("$user->sampul") ?>" alt="" class="profile-background">
            <div class="uploadPhoto" style="top: 110px;right: 20px;" id="coverPicture">
                <form action="<?= base_url('/akun/uploadsampul') ?>" enctype="multipart/form-data" method="POST">
                    <input type="file" id="uploadSampul" name="sampul" accept="image/*" onchange="clickHandler('submitSampul')" style="display:none">
                    <input type="submit" id="submitSampul" style="display:none">
                </form>
                <img class="upload" onclick="clickHandler('uploadSampul')" src="<?= base_url('assets/photo.png') ?>" alt="" width="20px" height="20px">
            </div>
            <img src="<?= base_url("$user->foto") ?>" alt="" class="profile-picture">
            <div class="uploadPhoto" style="top: 180px;left: 90px;" id="profilePicture">
                <form action="<?= base_url('/akun/uploadfoto') ?>" enctype="multipart/form-data" method="POST">
                    <input type="file" id="uploadFoto" name="foto" accept="image/*" onchange="clickHandler('submitFoto')" style="display:none">
                    <input type="submit" id="submitFoto" style="display:none">
                </form>
                <img class="upload" onclick="clickHandler('uploadFoto')" src="<?= base_url('assets/photo.png') ?>" alt="" width="20px" height="20px">
            </div>
            <div class="background">
                <h2 class="nama" style="color:black;"><?php echo $user->nama_lengkap ?></h2>
                <h6 class="username" style="color:grey;"><?php echo $user->username ?></h6>
            </div>
            <div id="mainContent">
                <?php foreach ($posts as $post) : ?>
                    <div class='postCard'>
                        <div class='profileSection'>
                            <img src='<?= base_url("$post->foto") ?>' alt='' class='profile-photo'>
                            <div class="accountIdentity">
                                <p class='accountName'><?php echo $user->nama_lengkap ?></p>
                                <p class="postTime"><?php echo $post->createdat ?></p>
                            </div>
                        </div>
                        <div class='post' style='margin-top:16px'>
                            <?php echo $post->post ?>
                        </div>

                        <!-- Like -->
						<div class="btn-like">
							<a href="<?= base_url('BerandaController/likeControl/' . $post->id_post); ?>">
								Sukai
								(<?php foreach ($post->likes as $like) {
										echo $like->total;
									} ?>)
							</a>
						</div>

                        <!-- Show Komentar -->
                        <div class="commentSection">
                            <?php foreach ($post->comments as $comment) : ?>
                                <div class="comment">
                                    <img src='<?= base_url("$comment->foto") ?>' alt='' class='comment-photo'>
                                    <div class="comment-id">
                                        <div>
                                            <p class="commentator"><?php echo $comment->nama_lengkap ?></p>
                                        </div>
                                        <div>
                                            <p class="comments"><?php echo $comment->komentar ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>

                        <!-- Tambah Komentar -->
                        <form action="<?= base_url('AkunController/add_comment/' . $post->id_post) ?>" method="post">
                            <div class="addCommentSection">
                                <img src='<?= base_url("$user->foto") ?>' alt='' class='profile-photo'>
                                <input name="comment" class='inputComment' type="text" placeholder="Tulis Komentar">
                                <!-- <div class="submitComment">Kirim</div> -->
                                <button type="submit" class="submitComment">Kirim</button>
                            </div>
                        </form>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-3" id="sidebarRight">
            <h4 style="color:white; margin:16px 0px; font-weight:bold">Daftar Teman</h4>
            <?php
            foreach ($friends as $friend) {
                echo "<div class='friendCard'>
                <img src='http://localhost/kon.co/$friend->foto' alt='' class='profile-photo'>
                <h5 class='name'>$friend->nama_lengkap</h5>
                </div>";
            }
            ?>
        </div>
    </div>
    <script src="<?= base_url('/js/sweetalert.js') ?>"></script>
    <script>
        const logout = document.getElementById('logout');
        logout.addEventListener('click', () => {
            Swal.fire({
                title: 'Anda Yakin?',
                text: "Anda akan keluar dari akun",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Keluar'
            }).then((result) => {
                if (result.value) window.open('<?= base_url('/logout') ?>', '_self')
            })
        })

        const clickHandler = element => {
            const btn = document.getElementById(element)
            btn.click()
        }
    </script>
</body>

</html>