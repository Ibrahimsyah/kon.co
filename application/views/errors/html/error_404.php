<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>404 Page Not Found</title>
	<link rel="stylesheet" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>">
	<style>
		.container{
			display: flex;
			flex-direction: column;
			justify-content: center;
			height: 100vh;
		}
		h4{
			text-align: center;
		}
		a{
			color: #13C0E5;
		}
	</style>
</head>

<body>
	<div class="container">
		<img class="mx-auto" src="<?= base_url('assets/not_found.svg') ?>" width="60%" alt="">
		<h4>Oops, anda tersesat. Tidak ada jalan lain selain <a href="<?=base_url('/beranda') ?>"><b>kembali ke Home</b></a></h4>
	</div>
</body>

</html>