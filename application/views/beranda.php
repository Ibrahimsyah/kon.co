<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Kon.co</title>
	<link rel="stylesheet" href="<?php echo base_url('bootstrap/css/bootstrap.min.css') ?>" />
	<link rel="stylesheet" href="<?php echo base_url('css/beranda.css') ?>" />
</head>

<body>
	<div class="row" style="margin: 0; height: 100%;">
		<div class="col-3">
			<div id="sidebarLeft">
				<a href="<?= base_url('/') ?>">
					<h3 class="title">Kon.co</h3>
				</a>
				<center>
					<img src='<?= base_url("$user->foto") ?>' alt='' class='profile-photo-user'>
					<h4 class="profile-username"><?php echo $user->nama_lengkap ?></h4>
				</center>
				<a class="gotoProfile" href='<?= base_url('/akun') ?>'>Profil Saya</a>
			</div>
		</div>
		<div class="col-6" style="padding:0">
			<div id="mainContent">
				<h2 class="title" style="color:black;">Beranda</h2>
				<form action="<?= base_url('BerandaController/add_post') ?>" method="post">
					<div class="postCard" style="position: relative;">
						<div style="display: flex;flex-direction: row; margin-bottom: 40px">
							<img src="<?= base_url("$user->foto") ?>" alt="" class="profile-photo">
							<textarea name="post" class="input-post" placeholder="Bagikan cerita anda ke teman"></textarea>
						</div>
						<button type="submit" class="btn-post">Posting</button>
					</div>
				</form>

				<?php foreach ($posts as $post) : ?>
					<div class='postCard'>
						<div class='profileSection'>
							<img src='<?= base_url("$post->foto") ?>' alt='' class='profile-photo'>
							<div class="accountIdentity">
								<p class='accountName'><?php echo $post->nama_lengkap ?></p>
								<p class="postTime"><?php echo $post->createdat ?></p>
							</div>
						</div>
						<div class='post' style='margin-top:16px'>
							<?php echo $post->post ?>
						</div>

						<!-- Like -->
						<div class="btn-like">
							<a href="<?= base_url('BerandaController/likeControl/' . $post->id_post); ?>">
								Sukai
								(<?php foreach ($post->likes as $like) {
										echo $like->total;
									} ?>)
							</a>
						</div>

						<!-- Show Komentar -->
						<div class="commentSection">
							<?php foreach ($post->comments as $comment) : ?>
								<div class="comment">
									<img src='<?= base_url("$comment->foto") ?>' alt='' class='comment-photo'>
									<div class="comment-id">
										<div>
											<p class="commentator"><?php echo $comment->nama_lengkap ?></p>
										</div>
										<div>
											<p class="comments"><?php echo $comment->komentar ?></p>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>

						<!-- Tambah Komentar -->
						<form action="<?= base_url('BerandaController/add_comment/' . $post->id_post) ?>" method="post">
							<div class="addCommentSection">
								<img src='<?= base_url("$user->foto") ?>' alt='' class='profile-photo'>
								<input name="comment" class='inputComment' type="text" placeholder="Tulis Komentar">
								<button type="submit" class="submitComment">Kirim</button>
							</div>
						</form>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
		<div class="col-3" id="sidebarRight">
			<h4 style="color:white; margin:16px 0px; font-weight:bold">Daftar Teman</h4>
			<?php
			foreach ($friends as $friend) {
				echo "<div class='friendCard'>
                <img src='".base_url("$friend->foto")."' alt='' class='profile-photo'>
                <h5 class='name'>$friend->nama_lengkap</h5>
                </div>";
			}
			?>
		</div>
	</div>
</body>

</html>
