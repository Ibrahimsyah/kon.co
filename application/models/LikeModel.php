<?php

class LikeModel extends CI_Model
{
	function likePost($id_post, $username)
	{
		$timestamp = (new DateTime("now"))->format('Y-m-d H:i:s');
		$this->load->database();
		return $this->db->query("insert into `like` values('$username', $id_post, '$timestamp')");
	}

	function dislikePost($id_post, $username)
	{
		$this->load->database();
		return $this->db->query("delete from `like` where username='$username' AND id_post=$id_post");
	}

	function cekLike($id_post, $username)
	{
		$this->load->database();
		$query = $this->db->query("select * from `like` where username='$username' AND `id_post` = '$id_post'");
		$result = $query->result();
		if (!empty($result)) {
			return 1;
		} else {
			return 0;
		}
	}

	function getLikeCountForPost($id_post)
	{
		$this->load->database();
		$query = $this->db->query("select count(*) total from `like` where id_post=$id_post");
		return $query->result();
	}
}
