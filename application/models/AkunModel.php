<?php

class AkunModel extends CI_Model {
    public function insertUser($user) {
        $this->load->database();
        if (!$this->db->insert('akun', $user)) {
            return $this->db->error();
        }
    }

    public function getAllFriends($user) {
        $this->load->database();
        $query = $this->db->query("SELECT nama_lengkap, foto FROM akun where not username = '$user'");
        return $query->result();
    }

    public function getDataUser($userLogin) {
        $this->load->database();
        $query = $this->db->query("SELECT * FROM akun WHERE username = '$userLogin' OR email ='$userLogin'");
        return $query->first_row();
    }

    public function updateFoto($username, $foto) {
        $this->load->database();
        return $this->db->query("UPDATE akun SET foto = '$foto' WHERE username = '$username'");
    }

    public function add_account($data) {
        $this->load->database();
        $this->db->insert('tbl_user', $data);
    }

    public function updateSampul($username, $sampul) {
        $this->load->database();
        return $this->db->query("UPDATE akun SET sampul = '$sampul' WHERE username = '$username'");
    }
}
