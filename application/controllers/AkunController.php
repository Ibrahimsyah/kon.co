<?php

class AkunController extends CI_Controller
{
    function index()
    {
        $this->load->helper('url');
        $this->load->library('session');
        if ($this->session->has_userdata('user')) {
            $this->load->model('AkunModel');
            $this->load->model('postModel');
            $this->load->model('commentModel');
            $this->load->model('LikeModel');
            $this->load->helper('url');
            $user = $_SESSION['user'];
            $userData = $this->AkunModel->getDataUser($user);
            $data['user'] = $userData;
            $friends = $this->AkunModel->getAllFriends($user);
            $data['friends'] = $friends;
            $posts = $this->postModel->getAllPostForUser($user);
            foreach ($posts as $post) {
                $comment = $this->commentModel->getComment($post->id_post);
                $post->comments = $comment;

                $like = $this->LikeModel->getLikeCountForPost($post->id_post);
				$post->likes = $like;
            }
            $data['posts'] = $posts;
            $this->load->view('akun', $data);
        } else {
            redirect(base_url('/'));
        }
    }

    //upload sampul
    function uploadSampul()
    {
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('AkunModel');
        $username = $this->session->userdata('user');
        $imgName = $username . '-sampul';
        $imgPath = './uploads/';
        $config['upload_path'] = $imgPath;
        $config['allowed_types'] = 'jpg|png|jpeg|ico';
        $config['max_size'] = '8192';
        $config['overwrite'] = true;
        $config['file_name'] = $imgName;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('sampul')) {
            echo $this->upload->display_errors();
        } else {
            $this->AkunModel->updateSampul($username, 'uploads/' . $imgName . $this->upload->data('file_ext'));
            redirect(base_url('/akun'));
        }
    }

    //upload foto
    function uploadFoto()
    {
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('AkunModel');
        $username = $this->session->userdata('user');
        $imgName = $username . '-foto';
        $imgPath = './uploads/';
        $config['upload_path'] = $imgPath;
        $config['allowed_types'] = 'jpg|png|jpeg|ico';
        $config['max_size'] = '8192';
        $config['overwrite'] = true;
        $config['file_name'] = $imgName;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload('foto')) {
            echo $this->upload->display_errors();
        } else {
            $this->AkunModel->updateFoto($username, 'uploads/' . $imgName . $this->upload->data('file_ext'));
            redirect(base_url('/akun'));
        }
    }

    // Komentar
    public function add_comment($id_post)
    {
        $this->load->library('session');
        $this->load->helper('url');

        $username = $_SESSION['user'];
        $komentar = $this->input->post('comment');

        if ($komentar != null) {
            $this->load->model('commentModel');
            $this->commentModel->commentPost($id_post, $username, $komentar);
        }

        redirect(base_url('/akun'), 'refresh');
    }
}
