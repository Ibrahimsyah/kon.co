<?php

class Migration extends CI_Controller {
    function index() {
        echo 'hello';
    }
    function up() {
        $this->load->database();
        $this->db->query('
        create table if not exists akun(
            username varchar(20) not null primary key,
            password varchar(200),
            email varchar(50) not null unique,
            nama_lengkap varchar(50),
            kelamin char(1),
            tanggal_lahir date,
            tanggal_join date,
			foto varchar(100),
			sampul varchar(100)
        );');
        $this->db->query('create table if not exists posting(
            id_post smallint not null primary key auto_increment,
            username varchar(20) references akun(username),
            post varchar(500),
            createdat timestamp
        );');
        $this->db->query('create table if not exists `like`(
            username varchar(20) not null references akun(username),
            id_post smallint not null references posting(id_post),
            likedat timestamp,
            primary key(username, id_post)
        );');
        $this->db->query('create table if not exists komentar(
            id_komentar smallint not null auto_increment,
            id_post smallint not null references posting(id_post),
            username varchar(20) not null references akun(username),
            komentar varchar(200),
            createdat timestamp,
            primary key(id_komentar, id_post, username)
        );');
        $hashedPass = password_hash('123456', PASSWORD_BCRYPT);
        $now = date("Y-m-d");
        $this->db->query("insert into akun values('superadmin', '$hashedPass', 'superadmin@kon.co', 'Admin Kon.co', 'L', '2020-05-05', '$now', '/uploads/superadmin-foto.png', '/uploads/superadmin-sampul.jpg')");
        $now = date("Y-m-d h:m:s");
        $this->db->query("insert into posting values('', 'superadmin', 'Ini adalah post pertamaku', '$now')");
        $this->db->query("insert into komentar values('', 1, 'superadmin', 'Ini adalah Komentarku', '$now')");
        echo "Migration Success";
    }
    function down() {
        $this->load->database();
        $this->db->query('drop table `like`;');
        $this->db->query('drop table komentar;');
        $this->db->query('drop table posting;');
        $this->db->query('drop table akun;');
        echo "Undo Migration Success";
    }
}
