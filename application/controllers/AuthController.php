<?php

class AuthController extends CI_Controller {
    public function index() {
        $this->load->helper('url');
        $this->load->library('session');
        if ($this->session->has_userdata('user')) {
            redirect(base_url('/beranda'));
        } else {
            $this->load->view('login_regist');
        }
    }

    public function registerAccount() {
        $namaLengkap = $this->input->post('namaLengkap');
        $username = strtolower($this->input->post('usernameRegister'));
        $email = strtolower($this->input->post('email'));
        $pass = $this->input->post('passwordRegister');
        $password = password_hash($pass, PASSWORD_BCRYPT);
        $tanggal = $this->input->post('tanggal');
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $jk = $this->input->post('jk');
        $tglLahir = date_format(date_create($tahun . "-" . $bulan . "-" . $tanggal), "Y-m-d");
        $dateNow = date("Y-m-d");
        $data = array(
            'username' => $username,
            'password' => $password,
            'email' => $email,
            'nama_lengkap' => $namaLengkap,
            'kelamin' => $jk,
            'tanggal_lahir' => $tglLahir,
            'tanggal_join' => $dateNow,
        );
        $this->load->model('AkunModel');
        $error = $this->AkunModel->insertUser($data);
        if (!$error) {
            $this->load->library('session');
            $this->load->helper('url');
            $this->session->set_userdata('user', $username);
            redirect(base_url('/beranda'));
        } else {
            $this->load->helper('url');
            $data['registerError'] = $error;
            return $this->load->view('login_regist', $data);
        }
    }

    public function loginAccount() {
        $this->load->model('AkunModel');
        $this->load->helper('url');
        $userLogin = $this->input->post('userLogin');
        $user = $this->AkunModel->getDataUser($userLogin);
        if ($user) {
            $passwordLogin = $this->input->post('passwordLogin');
            $userPassword = $user->password;
            if (password_verify($passwordLogin, $userPassword)) {
                $this->load->library('session');
                $this->session->set_userdata('user', $user->username);
                redirect(base_url('/beranda'));
            } else {
                $data['loginError'] = "Password Salah";
                return $this->load->view('login_regist', $data);
            }
        } else {
            $data['loginError'] = "Akun tidak terdaftar";
            return $this->load->view('login_regist', $data);
        }
    }

    public function logoutAccount() {
        $this->load->library('session');
        $this->load->helper('url');
        $this->session->unset_userdata('user');
        redirect(base_url('/'));
    }
}
