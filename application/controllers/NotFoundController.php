<?php

class NotFoundController extends CI_Controller{

    function index(){
        $this->load->helper('url');
        $this->output->set_status_header(404);
        $this->load->view('errors/html/error_404');
    }
}