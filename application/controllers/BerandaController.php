<?php

class BerandaController extends CI_Controller
{
	public function index()
	{
		$this->load->helper('url');
		$this->load->library('session');
		if ($this->session->has_userdata('user')) {

			$this->load->model('AkunModel');
			$this->load->model('postModel');
			$this->load->model('commentModel');
			$this->load->model('LikeModel');

			$this->load->helper('url');

			$user = $_SESSION['user'];

			$userData = $this->AkunModel->getDataUser($user);
			$data['user'] = $userData;
			$friends = $this->AkunModel->getAllFriends($user);
			$data['friends'] = $friends;
			$posts = $this->postModel->getAllPost();
			foreach ($posts as $post) {
				$comment = $this->commentModel->getComment($post->id_post);
				$post->comments = $comment;

				$like = $this->LikeModel->getLikeCountForPost($post->id_post);
				$post->likes = $like;
			}
			$data['posts'] = $posts;
			$this->load->view('beranda', $data);
		} else {
			redirect(base_url('/'));
		}
	}
	public function add_post()
	{
		$this->load->library('session');
		$this->load->helper('url');

		$username = $_SESSION['user'];
		$post = $this->input->post('post');

		if ($post != null) {
			$this->load->model('postModel');
			$this->postModel->insertPost($username, $post);
		}

		redirect(base_url('/'), 'refresh');
	}

	public function add_comment($id_post)
	{
		$this->load->library('session');
		$this->load->helper('url');

		$username = $_SESSION['user'];
		$komentar = $this->input->post('comment');

		if ($komentar != null) {
			$this->load->model('commentModel');
			$this->commentModel->commentPost($id_post, $username, $komentar);
		}

		redirect(base_url('/'), 'refresh');
	}

	public function likeControl($id_post)
	{
		$this->load->library('session');
		$this->load->helper('url');
		$this->load->model('LikeModel');

		$username = $_SESSION['user'];
		$cekLike = $this->LikeModel->cekLike($id_post, $username);

		if (empty($cekLike)) {
			$this->LikeModel->likePost($id_post, $username);
		} else {
			$this->LikeModel->dislikePost($id_post, $username);
		}
		redirect($_SERVER['HTTP_REFERER']);
	}
}
